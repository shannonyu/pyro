# To do:
# examine prepruned items to see what the most common cause is

import sys, math, time, heapq
import model
import log

## Everything is negative log (base 10) probabilities
# It's important that this be really big, in particular, bigger than -language_model.LOGZERO
IMPOSSIBLE = 999

# Search parameters
prune_threshold = -math.log10(0.1)
prune_limit = 100
# only postprune if above limit. Pharaoh = 1. This makes the algorithm
# order-sensitive.
lazy_threshold_postpruning = 1 
table_threshold = IMPOSSIBLE
table_limit = 100

# Output options
k_best = 1

## Coverage vectors
class Coverage:
    def __init__(self, c, nc=0, fu=0):
        if type(c) is int:
            self.c = (0,)*c # 0 = not covered, 1 = covered
        else:
            self.c = c
        self.n_covered = nc
        self.first_uncovered = fu

    def __add__(self, span):
        if span is None:
            return self
        i,j = span

        c = []
        for k in xrange(len(self.c)):
            if self.c[k] == 1:
                c.append(1)
            elif i<=k<=j-1:
                c.append(1)
            else:
                c.append(0)

        if self.first_uncovered < i:
            first_uncovered = self.first_uncovered
        else:
            first_uncovered = j
            while first_uncovered < len(c) and c[first_uncovered] > 0:
                first_uncovered += 1
        
        return Coverage(tuple(c), self.n_covered+j-i, first_uncovered)

    def __str__(self):
        return "".join(map(lambda x: ["_", "*"][x], self.c))

class Item(object):
    cur_serial = 0

    def __init__(self, fcovered=None, states=None, antecedent=None, transition=None, dcost=0.0, cost=0.0, goal=0):
        self.fcovered = fcovered
        self.states = states
        self.deductions = [(cost, Deduction(dcost, antecedent, transition))]
        self.best_deduction = self.deductions[0]

        self.serial = Item.cur_serial
        self.goal = goal
        self.n_best = None
        Item.cur_serial += 1

    def bin(self):
        # we store the goal in the last bin
        if self.goal:
            return self.fcovered.n_covered + 1
        else:
            return self.fcovered.n_covered

    def handle(self):
        '''Return a hashable representation. In a real implementation this shouldnt
        be necessary.'''
        if self.goal:
            return model.GOAL
        else:
            return (self.fcovered.c,self.states)

    def merge(self, item):
        '''Return true if the new item replaced the old one.'''
        # could limit this to have k_best elements
        self.deductions.extend(item.deductions)

        # best item may have changed, so update score
        if item.best_deduction[0] < self.best_deduction[0]:
            self.serial = item.serial
            self.best_deduction = item.best_deduction
            return 1
        else:
            return 0

    def find_n_best(self, n):
        '''Return value: how many were actually found'''
        # self.n_best is a list of n elements, each of the form (cost,ded_i,rank),
        # which tells where the i'th best derivation of this item came from:
        # the rank'th best derivation of the ded_i'th deduction.

        if self.n_best is None:
            self.n_best = []
            self.n_best_candidates = [None]*len(self.deductions)
            for ded_i in xrange(len(self.deductions)):
                (cost,ded) = self.deductions[ded_i]
                if ded.antecedent is not None:
                    self.n_best_candidates[ded_i] = (cost,ded_i,0)
                else:
                    self.n_best_candidates[ded_i] = (cost,ded_i,None)
            heapq.heapify(self.n_best_candidates)

        while len(self.n_best)<n and len(self.n_best_candidates)>0:
            # Get the next best and add it to the list
            (cost,ded_i,rank) = heapq.heappop(self.n_best_candidates)
            self.n_best.append((cost,ded_i,rank))

            # Replenish the candidate pool
            ded = self.deductions[ded_i][1]
            if ded.antecedent is not None:
                ant = ded.antecedent
                if ant.find_n_best(rank+2) >= rank+2:
                    heapq.heappush(self.n_best_candidates, (ant.n_best[rank+1][0]+ded.dcost, ded_i, rank+1))
            # else do nothing; axiom doesn't have a 2nd-best

        return len(self.n_best)
            
    def extract_derivation(self, i):
        """Return value is a list whose first element is a tuple (fcovered,states)
        and whose subsequent elements are transitions."""
        self.find_n_best(i+1)
        if i >= len(self.n_best):
            return None
        
        (cost,ded_i,rank) = self.n_best[i]
        deduction = self.deductions[ded_i][1]
        if deduction.antecedent is not None:
            (antcost,result) = deduction.antecedent.extract_derivation(rank)
            result.append(deduction.transition)
        else:
            result = [(self.fcovered,self.states)]
        return (cost,result)

    def __str__(self):
        if self.goal:
            return " ".join(["id "+str(self.serial), "Goal!"])
        else:
            return " ".join(["id "+str(self.serial), str(self.fcovered), str(self.states)])

class Deduction(object):
    __slots__ = 'transition', 'dcost', 'antecedent'
    def __init__(self, dcost = 0.0, antecedent=None, transition=None):
        self.transition = transition
        self.dcost = dcost
        self.antecedent = antecedent

    def __str__(self):
        if type(self.transition) is tuple:
            ((i,j),ewords) = self.transition[0:2]
            return "%d,%d -> %s" % (i, j, " ".join(ewords))
        elif self.transition is model.TO_GOAL:
            return "TO_GOAL"
        else:
            return ""

class Bin(object):
    def __init__(self, chart):
        # elements are (-totalcost, item) because we want the queue
        # to be worst-first
        self.queue = []
        self.index = {}
        self.dead = 0
        self.best = IMPOSSIBLE
        self.cutoff = IMPOSSIBLE
        self.chart = chart # just for accounting

    def add(self, item, totalcost):
        handle = item.handle()
        oldentry = self.index.get(handle, None)
        if oldentry is not None:
            (oldtotalcost, olditem) = oldentry
            oldtotalcost = -oldtotalcost
            
            if log.level >= 3:
                log.write("Merging: %s from %s with %s\n" % (str(item), str(item.deductions[0][1]), str(self.index[handle])))
            self.chart.merged += 1

            if totalcost < oldtotalcost:
                # Kill the old item and merge into the new one
                oldentry[1] = None # kill in self.queue
                self.dead += 1
                item.deductions.extend(olditem.deductions)

                # Add the new one
                entry = [-totalcost, item]
                heapq.heappush(self.queue, entry) 
                self.index[handle] = entry # displace old item
                self.best = min(self.best, totalcost)
            else:
                # Merge the new one into the old one
                olditem.deductions.extend(item.deductions)
        else:
            if log.level >= 3:
                log.write("Adding: %s from %s\n" % (str(item), str(item.deductions[0][1])))
            self.chart.added += 1
            entry = [-totalcost, item]
            heapq.heappush(self.queue, entry)
            self.index[handle] = entry
            self.best = min(self.best, totalcost)

        self.cutoff = self.best+prune_threshold

        self.prune()

    def prune(self):
        # This method is more fastidious than Pharaoh's. Pharaoh only
        # seems to do threshold pruning if the prune_limit is
        # exceeded.

        if len(self.queue)-self.dead == 0:
            return
        
        while len(self.queue)-self.dead > prune_limit or -self.queue[0][0] > self.cutoff:
            (negtotalcost, item) = heapq.heappop(self.queue)
            if item is None:
                self.dead -= 1
            else:
                del self.index[item.handle()]
                self.chart.pruned += 1

        if len(self.queue)-self.dead == prune_limit:
            self.cutoff = min(self.cutoff, -self.queue[0][0])

    def items(self):
        # Deliver the items best first. This is a slow sort but it
        # speeds the search
        items = self.index.values()
        items.sort()
        items.reverse()
        return items

class Chart(object):
    def __init__(self, fwords, models):
        Item.cur_serial = 0
        
        self.fwords = fwords
        self.models = models

        self.bins = [Bin(self) for i in xrange(len(fwords)+2)]
        self.added = self.merged = self.prepruned = self.pruned = 0
        self.prepruned_spans = 0
        self.table_size = self.table_pruned = 0
        
        for (weight,m) in self.models:
            m.input(fwords)
            
        if log.level >= 1:
            log.write("Precomputing phrases and score estimates...\n")

        self.phrases = [None]*(len(fwords)+1)
        self.future_costs = [None]*(len(fwords)+1)
        for i in xrange(0, len(fwords)+1):
            self.phrases[i] = [None]*(len(fwords)+1)
            self.future_costs[i] = [None]*(len(fwords)+1)
            for j in xrange(i+1, len(fwords)+1):
                ephrases = []
                
                # Allow models to add their own transitions
                # we get a list of tuples: the first member
                # is the phrase, and the rest are not our business
                # bug: check for duplicates
                for (weight,m) in self.models:
                    ephrases += m.generate_ephrases((i,j))
                    
                # Last resort for unknown French word: pass it through (even if it occurs in a longer phrase)
                if len(ephrases) == 0 and j-i == 1:
                    ephrases += [(tuple(fwords[i:j]),None)]

                if len(ephrases) > 0:
                    best_cost = IMPOSSIBLE
                    
                    new_ephrases = []
                    for ephrase in ephrases:
                        ewords = ephrase[0]
                        cost = 0.0
                        true_cost = 0.0
                        for (weight,m) in self.models:
                            dcost = weight * m.estimate_transition(((i,j),)+ephrase)
                            cost += dcost
                            if m.stateless:
                                true_cost += dcost
                            if log.level == 4:
                                log.writeln("Model estimate for %s, %s: %f" % (ephrase, (i,j), dcost))
                            
                        new_ephrases.append((cost,true_cost)+ephrase) # decorate
                        if cost < best_cost:
                            best_cost = cost
                    ephrases = new_ephrases

                    new_ephrases = []
                    # not sure if Pharaoh uses true best or running best
                    for ephrase in ephrases:
                        cost = ephrase[0]
                        if cost <= best_cost + table_threshold:
                            new_ephrases.append(ephrase)
                        else:
                            self.table_pruned += 1
                    ephrases = new_ephrases

                    if len(ephrases) > table_limit:
                        ephrases.sort() # this produces a different order from Pharaoh
                        self.table_pruned += len(ephrases)-table_limit
                        ephrases[table_limit:] = []

                    self.table_size += len(ephrases)

                    ephrases = [x[1:] for x in ephrases] # undecorate

                    self.phrases[i][j] = ephrases
                    self.future_costs[i][j] = best_cost
                else:
                    self.phrases[i][j] = []
                    self.future_costs[i][j] = IMPOSSIBLE

                for k in xrange(0, i):
                    if self.future_costs[k][j] > self.future_costs[k][i]+self.future_costs[i][j]:
                        self.future_costs[k][j] = self.future_costs[k][i]+self.future_costs[i][j]

        if log.level >= 1:
            log.write("Table size: %d, pruned: %d\n" % (self.table_size, self.table_pruned))

    def future_cost(self, fcovered):
        cost = 0.0
        i = 0
        j = 0

        # find holes (j,i)
        while j<len(self.fwords):
            while j<len(self.fwords) and fcovered.c[j]:
                j += 1
            i = j
            while i<len(self.fwords) and not fcovered.c[i]:
                i += 1
            if j<len(self.fwords):
                cost += self.future_costs[j][i]
            j = i

        return cost

    def goal(self):
        if self.bins[-1].index.has_key(model.GOAL):
            return self.bins[-1].index[model.GOAL][1]
        else:
            return None

    def add(self, item, totalcost):
        self.bins[item.bin()].add(item, totalcost)
        
    def add_axioms(self):
        states = tuple([m.initial for (weight, m) in self.models])
        axiom = Item(Coverage(len(self.fwords)), states)
        self.add(axiom, 0.0)

    def add_consequences(self, item):
        if item.goal:
            return

        cost = item.best_deduction[0]

        if item.fcovered.n_covered == len(self.fwords):
            # finish item
            dcost = 0.0
            for i in xrange(len(self.models)):
                (weight,m) = self.models[i]
                dcost += weight * m.transition(item.fcovered, item.states[i], model.TO_GOAL)[1]
            newcost = cost + dcost
            goal = Item(goal=1, fcovered=item.fcovered, antecedent=item, transition=model.TO_GOAL, dcost=dcost, cost=newcost)
            self.add(goal, newcost)
            return

        # precompute whether each model is stateless. Yes, it really
        # saves time, as I discovered by accident
        stateless = [m.stateless for (weight,m) in self.models]
        wordless = [m.wordless for (weight,m) in self.models]

        for m_i in xrange(len(self.models)):
            for (i,j) in self.models[m_i][1].generate_spans(item.fcovered, item.states[m_i]):
                if log.level >= 2:
                    log.writeln("Span %s,%s" % (i,j))
                if len(self.phrases[i][j]) > 0:
                    # these values are the same for any filling
                    newcovered = item.fcovered + (i,j)
                    newbin = newcovered.n_covered
                    base_future_cost = self.future_cost(newcovered)

                    base_dcost = 0.0
                    base_states = [None]*len(self.models)

                    for m_j in xrange(len(self.models)):
                        if wordless[m_j] and not stateless[m_j]:
                            (weight,m) = self.models[m_j]
                            (base_states[m_j], mdcost) = m.transition(item.fcovered, item.states[m_j], ((i,j),))
                            base_future_cost += weight * m.estimate_future(newcovered, base_states[m_j])
                            base_dcost += weight * mdcost

                    # This would be sound if we knew that all costs were
                    # positive
                    if 0 and cost+base_dcost+base_future_cost > self.bins[newbin].cutoff:
                        prepruned_spans += 1
                        continue
                    
                    for ephrase in self.phrases[i][j]:
                        ewords = ephrase[1]
                        dcost = base_dcost+ephrase[0]
                        future_cost = base_future_cost
                        newstates = base_states[:]
                        transition = ((i,j),)+ephrase[1:]
                        for m_j in xrange(len(self.models)):
                            if not stateless[m_j] and not wordless[m_j]:
                                (weight,m) = self.models[m_j]
                                (newstates[m_j], mdcost) = m.transition(item.fcovered, item.states[m_j], transition)
                                future_cost += weight * m.estimate_future(newcovered, newstates[m_j])
                                dcost += weight * mdcost
                                if log.level >= 4:
                                    log.write("model %d: %f\n" % (m_j, mdcost))
                        newcost = cost+dcost
                        newtotalcost = newcost+future_cost

                        if newtotalcost <= self.bins[newbin].cutoff:
                            newitem = Item(newcovered, tuple(newstates), item, transition, dcost, newcost)
                            self.add(newitem, newtotalcost)
                        else:
                            self.prepruned += 1
                            if log.level >= 4:
                                # create Item, print it, then throw it away
                                newitem = Item(newcovered, tuple(newstates), item, transition, dcost, newcost)
                                log.write("Threshold prepruning: %s, %f = %f + %f + %f\n" % (newitem, newtotalcost, cost, dcost, future_cost))
                            else:
                                Item.cur_serial += 1 # even though we didn't create the Item

class Decoder(object):
    def __init__(self):
        self.models = []

    def add_model(self, m, weight=1.0):
        self.models.append((weight, m))

    def set_weights(self, weights):
        if len(weights) != len(self.models):
            raise Exception("Number of weights does not match number of models")
        for i in xrange(len(self.models)):
            self.models[i] = (weights[i], self.models[i][1])

    def translate(self, fwords):
        # create chart with axiom
        start_time = time.time()
        if log.level >= 1:
            log.write("-"*65 + "\n")
            log.write("Read sentence: %s\n" % " ".join(fwords))
        chart = Chart(fwords, self.models)
        self.chart = chart # for debugging

        if log.level == 1:
            log.write("Translating")

        chart.add_axioms()

        for bin in xrange(len(self.chart.bins)):
            if log.level >= 2:
                log.write("=== Bin %d ===\n" % bin)
            chart.bins[bin].prune()

            for (totalcost, item) in chart.bins[bin].items():
                if log.level >= 2:
                    log.write("Considering %s\n" % str(item))
                chart.add_consequences(item)
            if log.level == 1:
                log.write(".")

        if log.level >= 1:
            log.write("\n%d added, %d merged, %d pruned, %d prepruned\n" % (chart.added, chart.merged, chart.pruned, chart.prepruned))

        goal = chart.goal()
        if goal is not None:
            result = []
            if log.level >= 1:
                log.write("Extracting derivation(s)...")
            for rank in xrange(k_best):
                kth_best = goal.extract_derivation(rank)
                if kth_best is None:
                    break
                (cost, deriv) = kth_best
                (fcovered,states) = deriv[0]
                ewords = []
                states = list(states)
                costs = [0.0]*len(self.models)
                for k in xrange(1,len(deriv)):
                    if deriv[k] is not model.TO_GOAL:
                        ((i,j),epwords) = deriv[k][0:2]
                        # reconstruct English
                        ewords.extend(epwords)
                        
                        # reconstruct component scores
                        for m_i in xrange(len(self.models)):
                            (weight, m) = self.models[m_i]
                            (states[m_i], mdcost) = m.transition(fcovered, states[m_i], deriv[k])
                            fcovered = fcovered + (i,j)
                            costs[m_i] += mdcost
                    else:
                        for m_i in xrange(len(self.models)):
                            (weight, m) = self.models[m_i]
                            costs[m_i] += m.transition(fcovered, states[m_i], model.TO_GOAL)[1]
                result.append({'hyp' : ewords, 'scores' : costs})

            if log.level >= 1:
                log.write("%f seconds\n" % (time.time()-start_time))
            
            return result
        else:
            if log.level >= 1:
                log.write("No translation found\n")
            return []

