import decoder
import model, phrase_table, language_model
#log.level = 1

# Create the decoder instance.
d = decoder.Decoder()

# Important: Pyro uses base-10 logs, whereas Moses uses natural logs.
# So for all the probability models (phrase table, language model), to
# convert the feature weights of a Moses model to be used here, you
# must multiply the weight by log(10) = 2.3026.

# Add a phrase table: first, read it from disk, then add one column
# from the table to the model. The usage is:
#   d.add_model(phrase_table.PhraseModel(column, default, pt), weight)
# where
#   - column is the 0-based column number (usually between 0 and 3)
#   - default is what value should be used in place of missing values
#   - weight is the feature weight
# The example phrase table has only one column, but real phrase tables
# usually have more, so you need to do this for each column.

pt = phrase_table.PhraseTable("sample-models/phrase-model/phrase-table")
d.add_model(phrase_table.PhraseModel(0, 0., pt), 1.)

# Add a language model
d.add_model(language_model.LanguageModel("sample-models/lm/europarl.srilm"), 1.)

# Add the word penalty
d.add_model(model.WordPenalty(), 1.)

# Add the distortion penalty. Because the distortion model also drives
# how the source sentence is scanned (which is, frankly, a bug), this
# model is not optional.
d.add_model(model.DistortionModel(max=4), 1.)

for line in open("sample-models/phrase-model/in"):
    fwords = line.split()
    result = d.translate(fwords)
    if len(result) > 0:
        print " ".join(result[0]['hyp'])
