#!/nfshomes/dchiang/pkg/python/bin/python

# Moses format:
# le chien ||| the dog ||| 0.123 0.234 0.345 ||| |||

import sys, math, gzip
import model, log

class PhraseTable(object):
    def __init__(self, read):
        self.table = {}
        if type(read) is str:
	    if read[-3:] == ".gz":
		self.read(gzip.GzipFile(read))
	    else:
		self.read(file(read))
        else:
            self.read(read)

    def clear(self):
        self.table.clear()
    
    def read(self, f):
        log.writeln("Reading phrase table...")
        self.clear()
        for line in f:
            if line == "": continue
            (fwords, ewords, weight) = line.split("|||")[:3]
            fwords = tuple(fwords.split())
            ewords = tuple(ewords.split())
            weight = [float(x) for x in weight.split()]
            self.table.setdefault(fwords, {})
            self.table[fwords][ewords] = weight

    def lookup(self, fwords, ewords=None):
        row = self.table.get(fwords, {})
        if ewords is not None:
            return row.get(ewords, None)
        else:
            return row.iteritems()

    def iteritems(self, sort=False):
        if sort:
            fphrases = self.table.keys()
            fphrases.sort()
            for fwords in fphrases:
                row = self.table[fwords].items()
                row.sort()
                for (ewords, weights) in row:
                    yield (fwords,ewords,weights)
        else:
            for fwords in self.table.iterkeys():
                for (ewords, weights) in self.table[fwords].iteritems():
                    yield (fwords,ewords,weights)

class PhraseModel(model.Model):
    def __init__(self, i, default, pt=None):
        model.Model.__init__(self)
        self.i = i
        self.default = default
        self.pt = pt
        self.stateless = 1

    def input (self, fwords):
        self.fwords = fwords

    def transition (self, mstate, state, transition):
        if transition is not model.TO_GOAL:
            #((i,j), ewords, pm_scores) = transition
            pm_scores = transition[2]
            if pm_scores is not None:
                return (None, pm_scores[self.i])
            else:
                return (None, self.default)
        return (None, 0.0)
        
    def estimate_transition (self, transition):
        ((i,j), ewords, pm_scores) = transition
        if pm_scores is not None:
            return pm_scores[self.i]
        else:
            return self.default

    def generate_ephrases (self, span):
        if self.pt is not None:
            (i,j) = span
            fpwords = tuple(self.fwords[i:j])
            row = self.pt.table.get(fpwords, {})
            for (ewords, pm_scores) in row.iteritems():
                yield (tuple(ewords), [-math.log10(x) for x in pm_scores])

if __name__ == "__main__":
    import fileinput
    pt = PhraseTable(fileinput.input())
